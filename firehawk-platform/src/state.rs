use firehawk_db::{PgPool, ScyllaSession};
use firehawk_prelude::*;

pub struct FirehawkState {
	pub pg: PgPool,
	pub scylla: ScyllaSession,
}

impl FirehawkState {
	pub async fn new(pg: PgPool, scylla: ScyllaSession) -> Result<Self> {
		Ok(Self { pg, scylla })
	}
}
