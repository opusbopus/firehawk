#![feature(async_fn_in_trait)]

use std::sync::Arc;

use firehawk_prelude::*;

use crate::state::FirehawkState;

pub mod state;

pub trait FirehawkPlatform: Sized {
	type Params;

	async fn init(
		state: Arc<FirehawkState>,
		params: &Self::Params,
	) -> Result<Self>;

	async fn start(&self) -> Result<()>;

	async fn shutdown(&self) -> Result<()>;
}
