use std::path::PathBuf;

use serde::{Deserialize, Serialize};

use crate::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct PostgresConfig {
	pub uri: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ScyllaConfig {
	pub uri: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DBConfig {
	pub scylla: ScyllaConfig,
	pub postgres: PostgresConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DiscordConfig {
	pub token: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FirehawkConfig {
	pub db: DBConfig,
	pub discord: DiscordConfig,
}

const DEFAULT_CONFIG: &str = r#"
[db.scylla]
uri = "localhost:9042"

[db.postgres]
uri = "postgres://postgres:postgres@127.0.0.1:5432/postgres"

[discord]
token = "<TOKEN>"
"#;

impl FirehawkConfig {
	pub async fn default() -> Result<Self> {
		let path = PathBuf::from("./config.toml");
		let contents = if path.exists() {
			tokio::fs::read_to_string(&path).await?
		} else {
			tokio::fs::write(&path, &DEFAULT_CONFIG).await?;
			panic!("Configuration file doesn't exist, please edit the generated config.")
		};

		Ok(toml::from_str(&contents)?)
	}
}
