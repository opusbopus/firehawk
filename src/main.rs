use firehawk_platform::FirehawkPlatform;
use firehawk_platform_discord::FirehawkPlatformDiscord;

use crate::{
	config::FirehawkConfig, prelude::*,
	termination_handler::termination_handler,
};

mod config;
mod prelude;
mod termination_handler;

#[tokio::main]
async fn main() -> Result<()> {
	color_eyre::install()?;
	tracing_subscriber::fmt()
		.with_max_level(Level::DEBUG)
		.init();

	let config = FirehawkConfig::default().await?;
	let state = {
		let pg = firehawk_db::get_postgres_pool(&config.db.postgres.uri)
			.await?;
		let scylla =
			firehawk_db::get_scylla_session(&config.db.scylla.uri).await?;
		let state = FirehawkState::new(pg, scylla).await?;
		Arc::new(state)
	};

	let discord_bot = FirehawkPlatformDiscord::init(
		state.clone(),
		&config.discord.token,
	)
	.await?;

	let termination_handler = termination_handler()?;
	let bot_future = discord_bot.start();

	tokio::pin!(termination_handler, bot_future);

	loop {
		tokio::select! {
			res = bot_future => {
				if let Err(err) = res {
					error!("Discord bot exited with an error!");
					error!("{err}");
				}

				break;
			}

			_ = termination_handler => {
				info!("Received termination signal.");
				break;
			}
		}
	}

	discord_bot.shutdown().await?;

	info!("Shutting down.");

	Ok(())
}
