pub use std::sync::Arc;

pub use firehawk_platform::state::FirehawkState;
pub use firehawk_prelude::*;
