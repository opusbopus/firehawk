use std::future::Future;

use crate::prelude::*;

pub fn termination_handler() -> Result<impl Future<Output = ()>> {
	#[cfg(unix)]
	use tokio::signal::unix::{signal, SignalKind};
	#[cfg(windows)]
	use tokio::signal::windows::{ctrl_c, ctrl_close};

	#[cfg(unix)]
	let mut interrupt = signal(SignalKind::interrupt())?;
	#[cfg(unix)]
	let mut terminate = signal(SignalKind::terminate())?;

	#[cfg(windows)]
	let mut interrupt = ctrl_c()?;
	#[cfg(windows)]
	let mut terminate = ctrl_close()?;

	let future = || async move {
		let interrupt_fut = interrupt.recv();
		let terminate_fut = terminate.recv();

		tokio::pin!(interrupt_fut);
		tokio::pin!(terminate_fut);

		loop {
			tokio::select! {
				_ = interrupt_fut => {
					break;
				}

				_ = terminate_fut => {
					break;
				}
			}
		}
	};

	Ok(future())
}
