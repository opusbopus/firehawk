pub use eyre::*;
pub use tracing::*;

pub mod chrono {
	pub use chrono::*;
}

pub trait OptionExt<T, E> {
	fn unwrap_or_err(self, err: E) -> Result<T, E>;
}

impl<T, E> OptionExt<T, E> for Option<T> {
	fn unwrap_or_err(self, err: E) -> Result<T, E> {
		self.map_or(Err(err), Ok)
	}
}
