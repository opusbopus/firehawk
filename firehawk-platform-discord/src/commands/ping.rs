use firehawk_prelude::*;

use crate::DiscordContext;

#[poise::command(prefix_command)]
pub async fn ping(ctx: DiscordContext<'_>) -> Result<()> {
	ctx.say("pong").await?;
	Ok(())
}
