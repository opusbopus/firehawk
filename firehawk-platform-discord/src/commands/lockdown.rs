use firehawk_prelude::*;
use poise::serenity_prelude::{
	Channel, PermissionOverwrite, PermissionOverwriteType, Permissions,
};

use crate::DiscordContext;

async fn update_everyone_overwrite(
	ctx: DiscordContext<'_>,
	transform: fn(&mut PermissionOverwrite),
) -> Result<()> {
	if let Some(guild) = ctx.guild() {
		let everyone_role = guild
			.role_by_name("@everyone")
			.unwrap_or_err(eyre!("@everyone doesn't exist? HUH"))?;

		for channel in guild.channels.values() {
			if let Channel::Guild(channel) = channel {
				let existing_permission_overwrites = channel.permission_overwrites.iter().filter(|v| {
					matches!(v.kind, PermissionOverwriteType::Role(id) if id == everyone_role.id)
				}).collect::<Vec<&PermissionOverwrite>>();
				let existing_permission_overwrites =
					existing_permission_overwrites.first().unwrap_or_err(
						eyre!(
							"No permission overwrites for @everyone? HUH"
						),
					)?;

				let mut new_permission_overwrites =
					(*existing_permission_overwrites).clone();

				transform(&mut new_permission_overwrites);

				channel
					.create_permission(ctx, &new_permission_overwrites)
					.await?;
			}
		}
	}

	Ok(())
}

#[poise::command(prefix_command)]
pub async fn lockdown(ctx: DiscordContext<'_>) -> Result<()> {
	update_everyone_overwrite(ctx, |v| {
		v.allow &= Permissions::SEND_MESSAGES;
		v.deny |= Permissions::SEND_MESSAGES;
	})
	.await?;
	ctx.say("Lockdown initiated").await?;
	Ok(())
}

#[poise::command(prefix_command)]
pub async fn unlockdown(ctx: DiscordContext<'_>) -> Result<()> {
	update_everyone_overwrite(ctx, |v| {
		// TODO: Ideally we set the permission back to its original state-
		// instead of setting it to neutral
		v.deny &= Permissions::SEND_MESSAGES;
	})
	.await?;
	ctx.say("Lockdown ended").await?;
	Ok(())
}
