#![feature(async_fn_in_trait)]

mod commands;
mod events;

use std::sync::Arc;

use firehawk_platform::{state::FirehawkState, FirehawkPlatform};
use firehawk_prelude::*;
use poise::{
	serenity_prelude::GatewayIntents, Framework, PrefixFrameworkOptions,
};

pub(crate) type DiscordFramework = Framework<Arc<FirehawkState>, Report>;

pub(crate) type DiscordContext<'a> =
	poise::Context<'a, Arc<FirehawkState>, Report>;

pub(crate) type DiscordContextF<'a> =
	poise::FrameworkContext<'a, Arc<FirehawkState>, Report>;

pub struct FirehawkPlatformDiscord {
	framework: Arc<DiscordFramework>,
}

impl FirehawkPlatform for FirehawkPlatformDiscord {
	type Params = String;

	async fn init(
		state: Arc<FirehawkState>,
		params: &Self::Params,
	) -> Result<Self> {
		let framework = Framework::builder()
			.options(poise::FrameworkOptions {
				commands: vec![
					commands::ping(),
					commands::lockdown(),
					commands::unlockdown(),
				],
				event_handler: |ctx, event, framework, state| {
					Box::pin(events::event_listener(
						ctx, event, framework, state,
					))
				},
				prefix_options: PrefixFrameworkOptions {
					mention_as_prefix: true,
					prefix: Some("~".into()),
					..Default::default()
				},
				..Default::default()
			})
			.token(params.to_string())
			.intents(
				GatewayIntents::GUILDS
					| GatewayIntents::GUILD_MEMBERS
					| GatewayIntents::GUILD_BANS
					| GatewayIntents::GUILD_INVITES
					| GatewayIntents::GUILD_VOICE_STATES
					| GatewayIntents::GUILD_MESSAGES
					| GatewayIntents::MESSAGE_CONTENT,
			)
			.setup(|ctx, _ready, framework| {
				Box::pin(async move {
					poise::builtins::register_globally(
						ctx,
						&framework.options().commands,
					)
					.await?;
					Ok(state)
				})
			});

		let framework = framework.build().await?;
		Ok(Self { framework })
	}

	async fn start(&self) -> Result<()> {
		self.framework.clone().start().await?;
		Ok(())
	}

	async fn shutdown(&self) -> Result<()> {
		self.framework
			.shard_manager()
			.lock()
			.await
			.shutdown_all()
			.await;
		Ok(())
	}
}
