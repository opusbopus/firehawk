use firehawk_db::models::{db::types::Platform, user::User};
use firehawk_platform::state::FirehawkState;
use firehawk_prelude::{chrono::Utc, *};
use poise::serenity_prelude::{Activity, Context};

use crate::DiscordContextF;

pub async fn event_listener(
	ctx: &Context,
	event: &poise::Event<'_>,
	_framework: DiscordContextF<'_>,
	state: &FirehawkState,
) -> Result<()> {
	match event {
		poise::Event::Ready { .. } => {
			ctx.set_activity(Activity::watching("you")).await;
		}
		poise::Event::Message { new_message }
			if !new_message.author.bot =>
		{
			User::ensure_exists(
				&state.pg,
				Platform::Discord,
				new_message.author.id.0 as i64,
				&new_message.author.name,
			)
			.await?
			.track_message(
				&state.scylla,
				Platform::Discord,
				&new_message.content,
				Utc::now().timestamp_millis(),
			)
			.await?;
		}
		_ => {}
	}
	Ok(())
}
