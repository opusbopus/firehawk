--- ARG 1 = The ID of the user on the specified platform
--- ARG 2 = The platform
SELECT u.*,
       array_remove(array_agg(l.*), NULL) "linked_accounts: Vec<DbLinkedAccount>",
       array_agg(a.alias)::TEXT[]         "aliases: Vec<String>",
       array_remove(array_agg(n.*), NULL) "notes: Vec<DbNote>",
       array_remove(array_agg(p.*), NULL) "punishments: Vec<DbPunishment>"

FROM users AS u
         LEFT JOIN linked_accounts AS l ON u.id = l.user_id
         LEFT JOIN aliases AS a ON u.id = a.user_id
         LEFT JOIN notes AS n ON u.id = n.user_id
         LEFT JOIN punishments AS p ON u.id = p.user_id

WHERE l.platform_id = $1 AND l.platform = $2

GROUP BY u.id
LIMIT 1;
