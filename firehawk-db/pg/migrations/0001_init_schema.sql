CREATE TABLE users
(
    id SERIAL NOT NULL UNIQUE
);

CREATE TABLE linked_accounts
(
    id           SERIAL                     NOT NULL UNIQUE PRIMARY KEY,
    user_id      INTEGER REFERENCES users (id)
        ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
    platform     platform                   NOT NULL,
    platform_id  BIGINT NOT NULL,
    moderator_id INTEGER REFERENCES users (id)
        ON UPDATE CASCADE ON DELETE CASCADE DEFAULT NULL
);

CREATE INDEX linked_accounts_common_search_idx ON linked_accounts (platform, platform_id);

CREATE TABLE aliases
(
    id      SERIAL                          NOT NULL UNIQUE,
    user_id INTEGER REFERENCES users (id)
        ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
    alias   TEXT                            NOT NULL UNIQUE
);

CREATE TABLE notes
(
    id           SERIAL                     NOT NULL UNIQUE,
    user_id      INTEGER REFERENCES users (id)
        ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
    moderator_id INTEGER REFERENCES users (id)
        ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
    data         TEXT                       NOT NULL
);

CREATE TABLE punishments
(
    id                SERIAL                NOT NULL UNIQUE,
    user_id           INTEGER REFERENCES users (id)
        ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
    moderator_id      INTEGER REFERENCES users (id)
        ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
    type              punishment_type       NOT NULL,
    executed_platform platform              NOT NULL,
    reason            TEXT                  NOT NULL,
    duration          INTEGER
);
