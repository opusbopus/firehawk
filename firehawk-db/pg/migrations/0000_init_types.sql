CREATE TYPE platform AS ENUM ('discord', 'twitch', 'fansly');

CREATE TYPE punishment_type AS ENUM (
    'warning',
    'kick',
    'ban');
