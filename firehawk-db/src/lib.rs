use firehawk_prelude::*;
pub use scylla::Session as ScyllaSession;
use scylla::SessionBuilder;
pub use sqlx::PgPool;

pub mod models;

pub async fn get_postgres_pool(uri: &str) -> Result<PgPool> {
	let pool = PgPool::connect(uri).await?;
	sqlx::migrate!("./pg/migrations").run(&pool).await?;
	Ok(pool)
}

pub async fn get_scylla_session(uri: &str) -> Result<ScyllaSession> {
	let session = SessionBuilder::new().known_node(uri).build().await?;

	static MIGRATIONS: [&str; 2] = [
		include_str!("../scylla/0000_keyspace.csql"),
		include_str!("../scylla/0001_init_tables.csql"),
	];

	for migration in MIGRATIONS {
		session.query(migration, ()).await?;
	}

	Ok(session)
}
