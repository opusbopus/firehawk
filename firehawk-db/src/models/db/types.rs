use std::ops::Deref;

use sqlx::{
	database::HasValueRef, error::BoxDynError, postgres::PgTypeInfo,
	Decode, Postgres, Type, ValueRef,
};
use strum::{Display, EnumString};

#[derive(Debug, Copy, Clone, sqlx::Type, Display, EnumString)]
#[sqlx(rename_all = "lowercase", type_name = "platform")]
pub enum Platform {
	Discord,
	Twitch,
	Fansly,
}

#[derive(Debug, Copy, Clone, sqlx::Type)]
#[sqlx(rename_all = "lowercase", type_name = "punishment_type")]
pub enum PunishmentType {
	Warn,
	Kick,
	Ban,
}

#[derive(Debug, Clone)]
pub struct ModeratorId(Option<i32>);

impl Deref for ModeratorId {
	type Target = Option<i32>;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl Type<Postgres> for ModeratorId {
	fn type_info() -> <Postgres as sqlx::Database>::TypeInfo {
		PgTypeInfo::with_name("INT4")
	}
}

impl<'r> Decode<'r, Postgres> for ModeratorId {
	fn decode(
		value: <Postgres as HasValueRef<'r>>::ValueRef,
	) -> Result<Self, BoxDynError> {
		if value.is_null() {
			Ok(Self(None))
		} else {
			Ok(Self(Some(i32::decode(value)?)))
		}
	}
}

#[derive(Debug, Clone, sqlx::Type)]
pub struct DbNote {
	pub id: i32,
	pub user_id: i32,
	pub moderator_id: ModeratorId,
	pub data: String,
}

#[derive(Debug, Clone, sqlx::Type)]
pub struct DbPunishment {
	pub id: i32,
	pub user_id: i32,
	pub moderator_id: ModeratorId,
	pub punishment_type: PunishmentType,
	pub executed_platform: Platform,
	pub reason: String,
	pub duration: i32,
}

#[derive(Debug, Clone, sqlx::Type)]
pub struct DbLinkedAccount {
	pub id: i32,
	pub user_id: i32,
	pub platform: Platform,
	pub platform_id: i64,
	pub moderator_id: ModeratorId,
}
