use std::str::FromStr;

use firehawk_prelude::*;
use futures_util::StreamExt;

use crate::{
	models::db::types::{DbLinkedAccount, DbNote, DbPunishment, Platform},
	PgPool, ScyllaSession,
};

#[derive(Debug, Clone)]
pub struct User {
	pub id: i32,
	pub linked_accounts: Vec<DbLinkedAccount>,
	pub aliases: Vec<String>,
	pub notes: Vec<DbNote>,
	pub punishments: Vec<DbPunishment>,
}

#[derive(Debug, Clone)]
pub struct UserMessage {
	pub timestamp: i64,
	pub user_id: i32,
	pub platform: Platform,
	pub data: String,
}

// impl From<DbUser> for User {
// 	fn from(value: DbUser) -> Self {
// 		Self {
// 			id: value.id,
// 			linked_accounts: value.linked_accounts.unwrap_or_default(),
// 			aliases: value.aliases.unwrap_or_default(),
// 			notes: value.notes.unwrap_or_default(),
// 			punishments: value.punishments.unwrap_or_default(),
// 		}
// 	}
// }

impl User {
	/// Ensures the user exists in our DB.
	/// If a user is found then it is returned else-
	/// it's created from the params.
	pub async fn ensure_exists(
		pg: &PgPool,
		platform: Platform,
		platform_id: i64,
		alias: &str,
	) -> Result<User> {
		if let Some(user) =
			Self::get_by_platform_id(pg, platform, platform_id).await?
		{
			return Ok(user);
		}

		let mut transaction = pg.begin().await?;

		let user_id =
			sqlx::query!("INSERT INTO users DEFAULT VALUES RETURNING id")
				.fetch_one(&mut transaction)
				.await?
				.id;

		sqlx::query!(
			"INSERT INTO aliases (user_id, alias) VALUES ($1, $2)",
			user_id,
			alias
		)
		.execute(&mut transaction)
		.await?;

		sqlx::query!(
			"INSERT INTO linked_accounts (user_id, platform, platform_id) VALUES ($1, $2, $3)",
			user_id,
			platform as Platform,
			platform_id
		).execute(&mut transaction).await?;

		transaction.commit().await?;

		Self::get_by_platform_id(pg, platform, platform_id)
			.await?
			.ok_or(eyre!("User doesn't exist after creation ??????"))
	}

	/// Tries to get a user by their [platform_id] on [platform]
	pub async fn get_by_platform_id(
		pg: &PgPool,
		platform: Platform,
		platform_id: i64,
	) -> Result<Option<User>> {
		let row = sqlx::query_file!(
			"pg/queries/user/get_by_platform_id.sql",
			platform_id,
			platform as Platform
		)
		.fetch_optional(pg)
		.await?;

		if let Some(row) = row {
			Ok(Some(User {
				id: row.id,
				linked_accounts: row.linked_accounts.unwrap_or_default(),
				aliases: row.aliases.unwrap_or_default(),
				notes: row.notes.unwrap_or_default(),
				punishments: row.punishments.unwrap_or_default(),
			}))
		} else {
			Ok(None)
		}
	}

	/// Adds a message to scylla.
	/// Should be called by platform implementations when-
	/// a user sends a message.
	pub async fn track_message(
		&self,
		scylla: &ScyllaSession,
		platform: Platform,
		message: &str,
		timestamp: i64,
	) -> Result<()> {
		scylla
			.query("INSERT INTO firehawk.messages (timestamp, user_id, platform, data) VALUES (?, ?, ?, ?)", (timestamp, &self.id, platform.to_string(), message))
			.await?;

		debug!("Logged message for {}", self.aliases[0]);

		Ok(())
	}

	/// Gets a Vec of every message a user has sent-
	/// on a [platform]
	pub async fn get_messages(
		&self,
		scylla: &ScyllaSession,
		platform: Platform,
	) -> Result<Vec<UserMessage>> {
		let mut rows =
			scylla
				.query_iter("SELECT timestamp, user_id, platform, data FROM firehawk.messages WHERE user_id = ? AND platform = ?", (&self.id, platform.to_string()))
				.await?
				.into_typed::<(i64, i32, String, String)>();

		let mut messages = Vec::new();

		while let Some(row) = rows.next().await {
			let (timestamp, user_id, platform, data) = row?;
			let platform = Platform::from_str(&platform)?;
			messages.push(UserMessage {
				timestamp,
				user_id,
				platform,
				data,
			});
		}

		Ok(messages)
	}
}
